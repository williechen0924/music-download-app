# Music download app

Design an iOS app that download MP3 music file and show its ID3 tag.

## To iOS APP Candidate
Hi ,Thanks for your interest in the position. We have an iOS APP exam before interview. 

Please finish it and commit your code to GitLab before interview. 

You may need the permission to commit and create branch, please provide your GitLab account to us.

## Requirements

* There are a **Text Field** and a **Button** that you need to show in the first view

* Enter the URL of dowloading file in the **Text Field**. And start downloading the file after pressing the **Button**. 
  (The example of URL : http://download.transcendcloud.com/MusicExample/sample.mp3, or you can upload the sample file to any cloud storage.)

* There are a **Progress Bar** and a **Label** that need to show during downloading. Both of the **Progress Bar** and the **Label** show the percentage of downloaded file. (0% to 100%)

* Present an information view of ID3 tag after file downloading as below.

    ![info view](info.png)

* The information of ID3 that you need show :

    1. Thumbnail (Album)
    2. Title
    3. Artist
    4. Album
    5. Year
    6. Genre

* Add a **Cancel** button on the information view that can turn back to the download view.

## Optional

* You can add more features in your **Music download** app, like : 

    * Keep the history of downloading

    * Download two or more files at the same time

    * **Pause**, **Resume**, and **Cancel** function with downloading task

    * Mini music player

    * Any feature

## Note

* You can't import or use any third-party **framework / library**.

